#ifndef NOTEPAD_H
#define NOTEPAD_H

#include <QMainWindow>

namespace Ui {
class NOtepad;
}

class NOtepad : public QMainWindow
{
    Q_OBJECT

public:
    explicit NOtepad(QWidget *parent = 0);
    ~NOtepad();

private:
    Ui::NOtepad *ui;
};

#endif // NOTEPAD_H
